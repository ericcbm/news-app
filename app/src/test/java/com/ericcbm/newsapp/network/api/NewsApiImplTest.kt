package com.ericcbm.newsapp.network.api

import android.accounts.NetworkErrorException
import com.ericcbm.newsapp.BuildConfig
import com.ericcbm.newsapp.R
import com.ericcbm.newsapp.dummydata.TestHeadlines
import com.ericcbm.newsapp.network.service.NewsService
import com.ericcbm.newsapp.utils.StringUtils
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class NewsApiImplTest {
    @Mock
    private lateinit var newsService: NewsService
    @Mock
    private lateinit var stringUtils: StringUtils

    private lateinit var underTest: NewsApi

    @Before
    fun setUp() {
        underTest = NewsApiImpl(newsService, stringUtils)
    }

    @Test
    fun `should get headlines correctly`() {
        val newsApiSource = "newsApiSource"

        given(stringUtils.getString(R.string.news_api_source)).willReturn(newsApiSource)
        given(newsService.getHeadlines(newsApiSource, BuildConfig.NEWS_API_KEY)).willReturn(Single.just(TestHeadlines.headlines))

        underTest.getHeadlines()
            .test()
            .assertValue(TestHeadlines.headlines)
            .assertComplete()

        verify(newsService).getHeadlines(newsApiSource, BuildConfig.NEWS_API_KEY)
    }

    @Test
    fun `should get error`() {
        val error = NetworkErrorException()
        val newsApiSource = "newsApiSource"

        given(stringUtils.getString(R.string.news_api_source)).willReturn(newsApiSource)
        given(newsService.getHeadlines(newsApiSource, BuildConfig.NEWS_API_KEY)).willReturn(Single.error(error))

        underTest.getHeadlines()
            .test()
            .assertError(error)
            .assertNotComplete()

        verify(newsService).getHeadlines(newsApiSource, BuildConfig.NEWS_API_KEY)
    }
}