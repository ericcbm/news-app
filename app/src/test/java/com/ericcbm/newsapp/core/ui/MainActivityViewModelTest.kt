package com.ericcbm.newsapp.core.ui

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.biometric.BiometricManager
import androidx.biometric.BiometricPrompt
import androidx.lifecycle.Observer
import com.ericcbm.newsapp.R
import com.ericcbm.newsapp.core.ui.model.MainUiState
import com.ericcbm.newsapp.utils.BiometricUtils
import com.ericcbm.newsapp.utils.StringUtils
import com.ericcbm.newsapp.utils.alertUtils.AlertUtils
import org.junit.After
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.BDDMockito.verify
import org.mockito.Mock
import org.mockito.Mockito.never
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class MainActivityViewModelTest {
    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Mock
    private lateinit var biometricUtils: BiometricUtils
    @Mock
    private lateinit var stringUtils: StringUtils
    @Mock
    private lateinit var alertUtils: AlertUtils
    @Mock
    private lateinit var uiStateObserver: Observer<MainUiState>
    @Mock
    private lateinit var biometricManager: BiometricManager
    @Mock
    private lateinit var biometricPrompt: BiometricPrompt

    private lateinit var underTest: MainActivityViewModel

    private fun initViewModel() {
        underTest = MainActivityViewModel(biometricManager, biometricUtils, stringUtils, alertUtils)
        underTest.uiState.observeForever(uiStateObserver)
    }

    @Test
    fun `given Biometric is Available, then prompt login`() {
        given(biometricUtils.isBiometricAvailable(biometricManager)).willReturn(true)

        initViewModel()

        verify(uiStateObserver).onChanged(MainUiState.PromptBiometricUiState)
    }

    @Test
    fun `given Biometric is not Available, then do nothing`() {
        given(biometricUtils.isBiometricAvailable(biometricManager)).willReturn(false)

        initViewModel()

        verify(uiStateObserver, never()).onChanged(MainUiState.PromptBiometricUiState)
    }

    @Test
    fun `should prompt biometric login`() {
        initViewModel()

        underTest.promptBiometricLogin(biometricPrompt)

        verify(biometricUtils).promptBiometricLogin(biometricPrompt)
    }

    @Test
    fun `given error, then show alert and finish`() {
        val error = "error"
        val errorMessage = "errorMessage"

        given(stringUtils.getString(R.string.biometric_login_error, error)).willReturn(errorMessage)

        initViewModel()
        underTest.onAuthenticationError(error)

        verify(alertUtils).show(errorMessage)
        verify(uiStateObserver).onChanged(MainUiState.FinishUiState)
    }

    @After
    fun tearDown() {
        underTest.uiState.removeObserver(uiStateObserver)
    }
}