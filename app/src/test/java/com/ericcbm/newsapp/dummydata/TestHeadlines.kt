package com.ericcbm.newsapp.dummydata

import com.ericcbm.newsapp.components.news.uimodel.NewsUiModel
import com.ericcbm.newsapp.network.model.Article
import com.ericcbm.newsapp.network.model.Headlines
import java.util.*

object TestHeadlines {

    val headlines = Headlines(listOf(Article("title", "description", "url", "imageUrl", Date(), "content")))

    val newsList = listOf(NewsUiModel("url", "imageUrl", "title", "description", "time", "content"))
}