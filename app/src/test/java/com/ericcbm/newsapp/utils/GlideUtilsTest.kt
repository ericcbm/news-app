package com.ericcbm.newsapp.utils

import android.graphics.drawable.Drawable
import android.widget.ImageView
import com.bumptech.glide.RequestBuilder
import com.bumptech.glide.RequestManager
import com.ericcbm.newsapp.R
import com.ericcbm.newsapp.utils.imageLoader.GlideUtils
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class GlideUtilsTest {
    @Mock
    private lateinit var requestManager: RequestManager
    @Mock
    private lateinit var requestBuilder: RequestBuilder<Drawable>
    @Mock
    private lateinit var imageView: ImageView

    private lateinit var underTest: GlideUtils

    @Before
    fun setUp() {
        underTest = GlideUtils(requestManager)
    }

    @Test
    fun `should load image correctly`() {
        val imageUrl = "imageUrl"

        given(requestManager.load(imageUrl)).willReturn(requestBuilder)
        given(requestBuilder.placeholder(R.drawable.placeholder)).willReturn(requestBuilder)

        underTest.load(imageUrl, imageView)

        verify(requestManager).load(imageUrl)
        verify(requestBuilder).placeholder(R.drawable.placeholder)
        verify(requestBuilder).into(imageView)
    }
}