package com.ericcbm.newsapp.utils

import com.ericcbm.newsapp.utils.DateUtils.DATE_FORMAT
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import java.text.SimpleDateFormat
import java.util.*

@RunWith(MockitoJUnitRunner::class)
class DateUtilsTest {

    @Test
    fun `should format date`() {
        val date = Date()
        val expected = SimpleDateFormat(DATE_FORMAT, Locale.getDefault()).format(date)

        val actual = DateUtils.format(date)

        assertEquals(expected, actual)
    }

    @Test
    fun `given null date, then return empty string`() {
        val expected = ""

        val actual = DateUtils.format(null)

        assertEquals(expected, actual)
    }
}