package com.ericcbm.newsapp.utils

import android.content.Context
import com.ericcbm.newsapp.R
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class StringUtilsTest {
    @Mock
    private lateinit var context: Context

    private lateinit var underTest: StringUtils

    @Before
    fun setUp() {
        underTest = StringUtils(context)
    }

    @Test
    fun `should get string`() {
        val stringId = R.string.biometric_prompt_title
        val expected = "text"

        given(context.getString(stringId)).willReturn(expected)

        val actual = underTest.getString(stringId)

        assertEquals(expected, actual)
    }

    @Test
    fun `should get formatted string`() {
        val stringId = R.string.biometric_prompt_title
        val arg1 = "arg1"
        val arg2 = "arg2"
        val expected = "formattedText"

        given(context.getString(stringId, arg1, arg2)).willReturn(expected)

        val actual = underTest.getString(stringId, arg1, arg2)

        assertEquals(expected, actual)
    }
}