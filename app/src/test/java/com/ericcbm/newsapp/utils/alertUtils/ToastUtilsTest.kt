package com.ericcbm.newsapp.utils.alertUtils

import android.widget.Toast
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class ToastUtilsTest {
    @Mock
    private lateinit var toast: Toast

    private lateinit var underTest: ToastUtils

    @Before
    fun setUp() {
        underTest = ToastUtils(toast)
    }

    @Test
    fun `should show toast`() {
        val text = "text"
        val duration = Toast.LENGTH_LONG
        underTest.show(text, duration)

        verify(toast).setText(text)
        verify(toast).setDuration(duration)
        verify(toast).show()
    }
}