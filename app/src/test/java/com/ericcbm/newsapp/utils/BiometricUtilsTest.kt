package com.ericcbm.newsapp.utils

import androidx.biometric.BiometricManager
import androidx.biometric.BiometricPrompt
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.BDDMockito.verify
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class BiometricUtilsTest {
    @Mock
    private lateinit var stringUtils: StringUtils
    @Mock
    private lateinit var biometricManager: BiometricManager
    @Mock
    private lateinit var biometricPrompt: BiometricPrompt

    private lateinit var underTest: BiometricUtils

    @Before
    fun setUp() {
        underTest = BiometricUtils(stringUtils)
    }

    @Test
    fun `given device supports biometrics, then return true`() {
        given(biometricManager.canAuthenticate(BiometricManager.Authenticators.BIOMETRIC_STRONG)).willReturn(BiometricManager.BIOMETRIC_SUCCESS)

        val actual = underTest.isBiometricAvailable(biometricManager)

        assertTrue(actual)
    }

    @Test
    fun `given device  doesn't support biometrics, then return false`() {
        given(biometricManager.canAuthenticate(BiometricManager.Authenticators.BIOMETRIC_STRONG)).willReturn(BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE)

        val actual = underTest.isBiometricAvailable(biometricManager)

        assertFalse(actual)
    }

    @Test
    fun `should prompt biometric login`() {
        val promptInfo = BiometricPrompt.PromptInfo.Builder()
            .setTitle("title")
            .setSubtitle("subtitle")
            .setNegativeButtonText("cancel")
            .build()

        underTest.promptBiometricLogin(biometricPrompt, promptInfo)


        verify(biometricPrompt).authenticate(promptInfo)
    }
}