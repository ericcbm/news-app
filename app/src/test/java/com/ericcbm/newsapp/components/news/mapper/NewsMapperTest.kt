package com.ericcbm.newsapp.components.news.mapper

import com.ericcbm.newsapp.dummydata.TestHeadlines.headlines
import com.ericcbm.newsapp.dummydata.TestHeadlines.newsList
import com.ericcbm.newsapp.utils.DateUtils
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class NewsMapperTest {
    @Mock
    private lateinit var dateUtils: DateUtils

    private lateinit var underTest: NewsMapper

    @Before
    fun setUp() {
        underTest = NewsMapper(dateUtils)
    }

    @Test
    fun `should map news list correctly`() {
        val expected = newsList

        given(dateUtils.format(headlines.articles[0].publishedAt)).willReturn(newsList[0].time)

        val actual = underTest.mapNewsList(headlines)

        assertEquals(expected, actual)
    }
}