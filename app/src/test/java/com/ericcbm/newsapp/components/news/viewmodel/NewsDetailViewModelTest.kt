package com.ericcbm.newsapp.components.news.viewmodel

import android.widget.ImageView
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.ericcbm.newsapp.components.news.uimodel.NewsUiModel
import com.ericcbm.newsapp.dummydata.TestHeadlines
import com.ericcbm.newsapp.utils.imageLoader.ImageLoader
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class NewsDetailViewModelTest {
    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Mock
    private lateinit var imageLoader: ImageLoader
    @Mock
    private lateinit var newsDetailObserver: Observer<NewsUiModel>
    @Mock
    private lateinit var imageView: ImageView

    private lateinit var underTest: NewsDetailViewModel

    @Before
    fun setUp() {
        underTest = NewsDetailViewModel(imageLoader)
        underTest.newsDetail.observeForever(newsDetailObserver)
    }

    @Test
    fun `given news detail, set value and load image view`() {
        val news = TestHeadlines.newsList[0]
        underTest.onNewsDetail(news, imageView)

        verify(newsDetailObserver).onChanged(news)
        verify(imageLoader).load(news.imageUrl, imageView)
    }

    @After
    fun tearDown() {
        underTest.newsDetail.removeObserver(newsDetailObserver)
    }
}