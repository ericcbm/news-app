package com.ericcbm.newsapp.components.news.repository

import android.accounts.NetworkErrorException
import com.ericcbm.newsapp.dummydata.TestHeadlines.headlines
import com.ericcbm.newsapp.network.api.NewsApi
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class NewsRepositoryTest {
    @Mock
    private lateinit var newsApi: NewsApi

    private lateinit var underTest: NewsRepository

    @Before
    fun setUp() {
        underTest = NewsRepository(newsApi)
    }

    @Test
    fun `should get headlines correctly`() {
        given(newsApi.getHeadlines()).willReturn(Single.just(headlines))

        underTest.getHeadlines()
            .test()
            .assertValue(headlines)
            .assertComplete()

        verify(newsApi).getHeadlines()
    }

    @Test
    fun `should get error`() {
        val error = NetworkErrorException()

        given(newsApi.getHeadlines()).willReturn(Single.error(error))

        underTest.getHeadlines()
            .test()
            .assertError(error)
            .assertNotComplete()

        verify(newsApi).getHeadlines()
    }
}