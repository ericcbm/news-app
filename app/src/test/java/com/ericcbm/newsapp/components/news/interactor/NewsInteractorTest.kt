package com.ericcbm.newsapp.components.news.interactor

import android.accounts.NetworkErrorException
import com.ericcbm.newsapp.any
import com.ericcbm.newsapp.components.news.mapper.NewsMapper
import com.ericcbm.newsapp.components.news.repository.NewsRepository
import com.ericcbm.newsapp.dummydata.TestHeadlines.headlines
import com.ericcbm.newsapp.dummydata.TestHeadlines.newsList
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.Mock
import org.mockito.Mockito.never
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class NewsInteractorTest {
    @Mock
    private lateinit var newsRepository: NewsRepository
    @Mock
    private lateinit var newsMapper: NewsMapper

    private lateinit var underTest: NewsInteractor

    @Before
    fun setUp() {
        underTest = NewsInteractor(newsRepository, newsMapper)
    }

    @Test
    fun `should get news list correctly`() {
        given(newsRepository.getHeadlines()).willReturn(Single.just(headlines))
        given(newsMapper.mapNewsList(headlines)).willReturn(newsList)

        underTest.getNewsList()
            .test()
            .assertValue(newsList)
            .assertComplete()

        verify(newsRepository).getHeadlines()
        verify(newsMapper).mapNewsList(headlines)
    }

    @Test
    fun `should get error`() {
        val error = NetworkErrorException()

        given(newsRepository.getHeadlines()).willReturn(Single.error(error))

        underTest.getNewsList()
            .test()
            .assertError(error)
            .assertNotComplete()

        verify(newsRepository).getHeadlines()
        verify(newsMapper, never()).mapNewsList(any())
    }
}