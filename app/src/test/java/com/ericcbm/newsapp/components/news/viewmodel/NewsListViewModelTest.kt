package com.ericcbm.newsapp.components.news.viewmodel

import android.accounts.NetworkErrorException
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.ericcbm.newsapp.RxSchedulerRule
import com.ericcbm.newsapp.components.news.interactor.NewsInteractor
import com.ericcbm.newsapp.components.news.model.UiState
import com.ericcbm.newsapp.components.news.uimodel.NewsUiModel
import com.ericcbm.newsapp.dummydata.TestHeadlines.newsList
import io.reactivex.Single
import org.junit.After
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.*
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class NewsListViewModelTest {
    @get:Rule
    val rule = InstantTaskExecutorRule()
    @get:Rule
    val rxSchedulerRule = RxSchedulerRule()

    @Mock
    private lateinit var newsInteractor: NewsInteractor
    @Mock
    private lateinit var newsListObserver: Observer<List<NewsUiModel>>
    @Mock
    private lateinit var uiStateObserver: Observer<UiState>

    private lateinit var underTest: NewsListViewModel

    private fun initViewModel() {
        underTest = NewsListViewModel(newsInteractor)

        underTest.newsList.observeForever(newsListObserver)
        underTest.uiState.observeForever(uiStateObserver)
    }

    @Test
    fun `should get news list on view model initialization`() {
        given(newsInteractor.getNewsList()).willReturn(Single.just(newsList))

        initViewModel()

        verify(newsInteractor).getNewsList()
        verify(uiStateObserver).onChanged(UiState.LoadingUiState)
        verify(newsListObserver).onChanged(newsList)
    }

    @Test
    fun `should show error view when api call error`() {
        given(newsInteractor.getNewsList()).willReturn(Single.error(NetworkErrorException()))

        initViewModel()

        verify(newsInteractor).getNewsList()
        verify(uiStateObserver).onChanged(UiState.ErrorUiState)
        verify(newsListObserver, never()).onChanged(any())
    }

    @After
    fun tearDown() {
        underTest.newsList.removeObserver(newsListObserver)
        underTest.uiState.removeObserver(uiStateObserver)
    }
}