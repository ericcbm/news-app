package com.ericcbm.newsapp.utils.imageLoader

import android.widget.ImageView
import com.bumptech.glide.RequestManager
import com.ericcbm.newsapp.R

class GlideUtils(private val requestManager: RequestManager): ImageLoader {
    override fun load(imageUrl: String?, imageView: ImageView) {
        requestManager
            .load(imageUrl)
            .placeholder(R.drawable.placeholder)
            .into(imageView)
    }
}