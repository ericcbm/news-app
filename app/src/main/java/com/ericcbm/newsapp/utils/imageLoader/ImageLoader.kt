package com.ericcbm.newsapp.utils.imageLoader

import android.widget.ImageView

interface ImageLoader {
    fun load(imageUrl: String?, imageView: ImageView)
}