package com.ericcbm.newsapp.utils

import android.view.View.GONE
import android.widget.TextView

fun TextView.setTextOrHide(value: String?) = when {
    value.isNullOrEmpty() -> visibility = GONE
    else -> text = value
}