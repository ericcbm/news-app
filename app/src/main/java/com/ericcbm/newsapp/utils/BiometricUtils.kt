package com.ericcbm.newsapp.utils

import androidx.biometric.BiometricManager
import androidx.biometric.BiometricPrompt
import com.ericcbm.newsapp.R

class BiometricUtils(private val stringUtils: StringUtils) {

    fun isBiometricAvailable(biometricManager: BiometricManager) =
        when (biometricManager.canAuthenticate(BiometricManager.Authenticators.BIOMETRIC_STRONG)) {
            BiometricManager.BIOMETRIC_SUCCESS -> true
            else -> false
        }

    fun promptBiometricLogin(biometricPrompt: BiometricPrompt, promptInfo: BiometricPrompt.PromptInfo = getPromptInfo()) {
        biometricPrompt.authenticate(promptInfo)
    }

    private fun getPromptInfo() = BiometricPrompt.PromptInfo.Builder()
        .setTitle(stringUtils.getString(R.string.biometric_prompt_title))
        .setSubtitle(stringUtils.getString(R.string.biometric_prompt_subtitle))
        .setNegativeButtonText(stringUtils.getString(R.string.biometric_prompt_cancel))
        .setAllowedAuthenticators(BiometricManager.Authenticators.BIOMETRIC_STRONG)
        .build()
}