package com.ericcbm.newsapp.utils

import android.content.Context
import androidx.annotation.StringRes

class StringUtils(private val context: Context) {
    fun getString(@StringRes stringId: Int) = context.getString(stringId)
    fun getString(@StringRes stringId: Int, vararg args: Any) = context.getString(stringId, *args)
}