package com.ericcbm.newsapp.utils

import android.widget.Toast
import com.bumptech.glide.Glide
import com.ericcbm.newsapp.utils.alertUtils.AlertUtils
import com.ericcbm.newsapp.utils.alertUtils.ToastUtils
import com.ericcbm.newsapp.utils.imageLoader.GlideUtils
import com.ericcbm.newsapp.utils.imageLoader.ImageLoader
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val utilsModule = module {
    single { DateUtils }
    single { GlideUtils(Glide.with(androidContext())) as ImageLoader }
    single { BiometricUtils(get()) }
    single { StringUtils(androidContext()) }
    single { ToastUtils(Toast(androidContext())) as AlertUtils }
}