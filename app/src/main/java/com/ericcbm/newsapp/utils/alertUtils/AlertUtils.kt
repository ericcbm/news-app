package com.ericcbm.newsapp.utils.alertUtils

import android.widget.Toast

interface AlertUtils {
    fun show(string: CharSequence, duration: Int = Toast.LENGTH_LONG)
}