package com.ericcbm.newsapp.utils.alertUtils

import android.widget.Toast

class ToastUtils(private val toast: Toast): AlertUtils {
    override fun show(string: CharSequence, duration: Int) {
        toast.apply {
            setText(string)
            setDuration(duration)
            show()
        }
    }
}