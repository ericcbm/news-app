package com.ericcbm.newsapp.utils

import java.text.SimpleDateFormat
import java.util.*

object DateUtils {

    const val DATE_FORMAT = "EEE, d MMM yyyy HH:mm"

    fun format(date: Date?, dateFormat: String = DATE_FORMAT): String =
        date?.let { SimpleDateFormat(dateFormat, Locale.getDefault()).format(it) } ?: ""
}