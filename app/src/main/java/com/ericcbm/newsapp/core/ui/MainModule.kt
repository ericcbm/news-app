package com.ericcbm.newsapp.core.ui

import androidx.biometric.BiometricManager
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val mainModule = module {
    single { BiometricManager.from(androidContext()) }
    viewModel { MainActivityViewModel(get(), get(), get(), get()) }
}