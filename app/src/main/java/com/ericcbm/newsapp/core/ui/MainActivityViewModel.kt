package com.ericcbm.newsapp.core.ui

import androidx.biometric.BiometricManager
import androidx.biometric.BiometricPrompt
import androidx.lifecycle.ViewModel
import com.ericcbm.newsapp.R
import com.ericcbm.newsapp.core.livedata.SingleLiveEvent
import com.ericcbm.newsapp.core.ui.model.MainUiState
import com.ericcbm.newsapp.utils.BiometricUtils
import com.ericcbm.newsapp.utils.StringUtils
import com.ericcbm.newsapp.utils.alertUtils.AlertUtils

class MainActivityViewModel(private val biometricManager: BiometricManager,
                            private val biometricUtils: BiometricUtils,
                            private val stringUtils: StringUtils,
                            private val alertUtils: AlertUtils
) : ViewModel() {

    private val _uiState = SingleLiveEvent<MainUiState>()
    val uiState = _uiState

    init {
        showBiometricLoginIfAvailable()
    }

    private fun showBiometricLoginIfAvailable() {
        if (biometricUtils.isBiometricAvailable(biometricManager)) {
            uiState.value = MainUiState.PromptBiometricUiState
        }
    }

    fun promptBiometricLogin(biometricPrompt: BiometricPrompt) {
        biometricUtils.promptBiometricLogin(biometricPrompt)
    }

    fun onAuthenticationError(error: CharSequence) {
        alertUtils.show(stringUtils.getString(R.string.biometric_login_error, error))
        uiState.value = MainUiState.FinishUiState
    }
}