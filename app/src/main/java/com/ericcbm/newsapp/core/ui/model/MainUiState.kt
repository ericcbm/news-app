package com.ericcbm.newsapp.core.ui.model

sealed class MainUiState {
    object FinishUiState : MainUiState()
    object PromptBiometricUiState : MainUiState()
}