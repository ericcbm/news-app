package com.ericcbm.newsapp.core

import android.app.Application
import com.ericcbm.newsapp.components.news.module.newsModule
import com.ericcbm.newsapp.core.ui.mainModule
import com.ericcbm.newsapp.network.module.newsApiModule
import com.ericcbm.newsapp.utils.utilsModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.GlobalContext.startKoin

class MyApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin{
            androidLogger()
            androidContext(this@MyApplication)
            modules(utilsModule, newsApiModule, newsModule, newsModule, mainModule)
        }
    }
}