package com.ericcbm.newsapp.core.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.biometric.BiometricPrompt
import androidx.core.content.ContextCompat
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI
import com.ericcbm.newsapp.R
import com.ericcbm.newsapp.core.ui.model.MainUiState
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity() {

    private val viewModel: MainActivityViewModel by viewModel()
    private val navController by lazy { (supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment).navController }
    private val appBarConfiguration by lazy { AppBarConfiguration.Builder(navController.graph).build() }
    private val biometricPrompt by lazy {
        BiometricPrompt(this, ContextCompat.getMainExecutor(this),
            object : BiometricPrompt.AuthenticationCallback() {
                override fun onAuthenticationError(errorCode: Int, errString: CharSequence) {
                    super.onAuthenticationError(errorCode, errString)
                    viewModel.onAuthenticationError(errString)
                }
            })
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration)
        viewModel.apply {
            uiState.observe(this@MainActivity, {
                when (it) {
                    MainUiState.PromptBiometricUiState -> promptBiometricLogin(biometricPrompt)
                    MainUiState.FinishUiState -> finish()
                    else -> { /* no-op */ }
                }
            })
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        NavigationUI.navigateUp(navController, appBarConfiguration)
        return super.onSupportNavigateUp()
    }
}