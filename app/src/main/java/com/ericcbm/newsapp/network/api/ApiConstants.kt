package com.ericcbm.newsapp.network.api

const val NEWS_API_BASE_URL = "https://newsapi.org/"
const val API_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"

const val API_PATH_HEADLINES = "v2/top-headlines"



