package com.ericcbm.newsapp.network.api

import com.ericcbm.newsapp.network.model.Headlines
import io.reactivex.Single

interface NewsApi {
    fun getHeadlines(): Single<Headlines>
}