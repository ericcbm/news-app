package com.ericcbm.newsapp.network.service

import com.ericcbm.newsapp.network.api.API_PATH_HEADLINES
import com.ericcbm.newsapp.network.model.Headlines
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface NewsService {
    @GET(API_PATH_HEADLINES)
    fun getHeadlines(@Query(value = "sources") sources: String, @Query(value = "apiKey") apiKey: String): Single<Headlines>
}