package com.ericcbm.newsapp.network.model

import com.ericcbm.newsapp.network.api.API_DATE_FORMAT
import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import java.util.*

@JsonIgnoreProperties(ignoreUnknown = true)
data class Article(
    @JsonProperty("title")
    val title: String?,
    @JsonProperty("description")
    val description: String?,
    @JsonProperty("url")
    val url: String,
    @JsonProperty("urlToImage")
    val imageUrl: String?,
    @JsonProperty("publishedAt")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = API_DATE_FORMAT)
    val publishedAt: Date?,
    @JsonProperty("content")
    val content: String?
)
