package com.ericcbm.newsapp.network.api

import com.ericcbm.newsapp.BuildConfig
import com.ericcbm.newsapp.R
import com.ericcbm.newsapp.network.model.Headlines
import com.ericcbm.newsapp.network.service.NewsService
import com.ericcbm.newsapp.utils.StringUtils
import io.reactivex.Single

class NewsApiImpl(private val newsService: NewsService, private val stringUtils: StringUtils): NewsApi {
    override fun getHeadlines(): Single<Headlines> = newsService.getHeadlines(stringUtils.getString(R.string.news_api_source), BuildConfig.NEWS_API_KEY)
}