package com.ericcbm.newsapp.network.module

import androidx.viewbinding.BuildConfig
import com.ericcbm.newsapp.network.api.NEWS_API_BASE_URL
import com.ericcbm.newsapp.network.api.NewsApi
import com.ericcbm.newsapp.network.api.NewsApiImpl
import com.ericcbm.newsapp.network.service.NewsService
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.jackson.JacksonConverterFactory

private val okHttpClient by lazy {
    OkHttpClient().newBuilder().let {
        if (BuildConfig.DEBUG) {
            it.addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
        } else {
            it
        }.build()
    }
}

private val newsService by lazy {
    Retrofit.Builder()
        .baseUrl(NEWS_API_BASE_URL)
        .addConverterFactory(JacksonConverterFactory.create())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .client(okHttpClient)
        .build().create(NewsService::class.java)
}

val newsApiModule = module {
    single<NewsService> { newsService }
    single { NewsApiImpl(get(), get()) as NewsApi }
}
