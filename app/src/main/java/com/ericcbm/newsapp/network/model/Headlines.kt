package com.ericcbm.newsapp.network.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
data class Headlines(
    @JsonProperty("articles")
    val articles: List<Article>
)
