package com.ericcbm.newsapp.components.news.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.ericcbm.newsapp.BuildConfig
import com.ericcbm.newsapp.components.news.interactor.NewsInteractor
import com.ericcbm.newsapp.components.news.model.UiState
import com.ericcbm.newsapp.components.news.uimodel.NewsUiModel
import com.ericcbm.newsapp.core.livedata.SingleLiveEvent
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class NewsListViewModel(private val newsInteractor: NewsInteractor) : ViewModel() {

    private val disposables: CompositeDisposable = CompositeDisposable()

    private val _newsList = MutableLiveData<List<NewsUiModel>>()
    val newsList by lazy {
        loadNewsList()
        _newsList
    }

    private val _uiState = SingleLiveEvent<UiState>()
    val uiState = _uiState

    fun loadNewsList() {
        newsInteractor.getNewsList()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { _uiState.value = UiState.LoadingUiState }
            .subscribe(::handleSuccess, ::handleError)
            .also { disposables.add(it) }
    }

    private fun handleError(throwable: Throwable?) {
        if (BuildConfig.DEBUG) {
            throwable?.printStackTrace()
        }
        _uiState.value = UiState.ErrorUiState
    }

    private fun handleSuccess(list: List<NewsUiModel>) {
        _newsList.value = list
    }

    override fun onCleared() {
        super.onCleared()
        disposables.dispose()
    }
}