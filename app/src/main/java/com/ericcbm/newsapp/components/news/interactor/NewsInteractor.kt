package com.ericcbm.newsapp.components.news.interactor

import com.ericcbm.newsapp.components.news.mapper.NewsMapper
import com.ericcbm.newsapp.components.news.repository.NewsRepository
import com.ericcbm.newsapp.components.news.uimodel.NewsUiModel
import io.reactivex.Single

class NewsInteractor(
    private val newsRepository: NewsRepository,
    private val newsMapper: NewsMapper
) {
    fun getNewsList(): Single<List<NewsUiModel>> =
        newsRepository.getHeadlines().map {
            newsMapper.mapNewsList(it)
        }
}