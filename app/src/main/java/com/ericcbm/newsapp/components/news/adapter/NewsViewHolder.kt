package com.ericcbm.newsapp.components.news.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.ericcbm.newsapp.components.news.uimodel.NewsUiModel
import com.ericcbm.newsapp.databinding.NewsHeadlineBinding
import com.ericcbm.newsapp.databinding.NewsItemBinding
import com.ericcbm.newsapp.utils.imageLoader.ImageLoader
import com.ericcbm.newsapp.utils.setTextOrHide
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

class NewsViewHolder(binding: NewsItemBinding, mergeBinding: NewsHeadlineBinding, val onClick: (View, NewsUiModel) -> Unit) : RecyclerView.ViewHolder(binding.root), KoinComponent {

    private val imageLoader: ImageLoader by inject()
    private val root by lazy { binding.root }
    private val newsImage by lazy { mergeBinding.newsItemImage }
    private val coinTitle by lazy { mergeBinding.newsItemTitle }
    private val coinTime by lazy { mergeBinding.newsItemTime }
    private var currentNews: NewsUiModel? = null

    init {
        root.setOnClickListener {
            currentNews?.let {
                onClick(root, it)
            }
        }
    }

    fun bind(news: NewsUiModel) {
        currentNews = news
        imageLoader.load(news.imageUrl, newsImage)
        coinTitle.setTextOrHide(news.title)
        coinTime.setTextOrHide(news.time)
    }
}