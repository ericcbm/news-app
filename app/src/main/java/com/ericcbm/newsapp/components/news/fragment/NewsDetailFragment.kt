package com.ericcbm.newsapp.components.news.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.ericcbm.newsapp.R
import com.ericcbm.newsapp.components.news.uimodel.NewsUiModel
import com.ericcbm.newsapp.components.news.viewmodel.NewsDetailViewModel
import com.ericcbm.newsapp.databinding.NewsDetailFragmentBinding
import com.ericcbm.newsapp.databinding.NewsHeadlineBinding
import com.ericcbm.newsapp.utils.setTextOrHide
import org.koin.androidx.viewmodel.ext.android.viewModel

class NewsDetailFragment : Fragment(R.layout.news_detail_fragment) {

    private var _binding: NewsDetailFragmentBinding? = null
    private var _bindingMerge: NewsHeadlineBinding? = null
    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!
    private val bindingMerge get() = _bindingMerge!!

    private val viewModel: NewsDetailViewModel by viewModel()
    private val args: NewsDetailFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = NewsDetailFragmentBinding.inflate(inflater, container, false)
        _bindingMerge = NewsHeadlineBinding.bind(binding.root)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.onNewsDetail(args.newsInfo, bindingMerge.newsItemImage)
        viewModel.newsDetail.observe(viewLifecycleOwner) {
            setToolbarTitle(it.title)
            setViewsValues(it)
        }
    }

    private fun setViewsValues(news: NewsUiModel) {
        binding.apply {
            newsDetailDescription.setTextOrHide(news.description)
            newsDetailBody.setTextOrHide(news.content)
        }
        bindingMerge.apply {
            newsItemTitle.setTextOrHide(news.title)
            newsItemTime.setTextOrHide(news.time)
        }
    }

    private fun setToolbarTitle(title: String?) {
        val actionBar = (activity as AppCompatActivity).supportActionBar
        actionBar?.title = title
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
        _bindingMerge = null
    }
}