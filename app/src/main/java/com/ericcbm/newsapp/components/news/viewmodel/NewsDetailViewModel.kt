package com.ericcbm.newsapp.components.news.viewmodel

import android.widget.ImageView
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.ericcbm.newsapp.components.news.uimodel.NewsUiModel
import com.ericcbm.newsapp.utils.imageLoader.ImageLoader

class NewsDetailViewModel(private val imageLoader: ImageLoader) : ViewModel() {

    private val _newsDetail = MutableLiveData<NewsUiModel>()
    val newsDetail = _newsDetail

    fun onNewsDetail(news: NewsUiModel, imageView: ImageView) {
        _newsDetail.value = news
        imageLoader.load(news.imageUrl, imageView)
    }
}