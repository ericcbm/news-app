package com.ericcbm.newsapp.components.news.uimodel

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class NewsUiModel(
    val url: String,
    val imageUrl: String?,
    val title: String?,
    val description: String?,
    val time: String?,
    val content: String?
): Parcelable