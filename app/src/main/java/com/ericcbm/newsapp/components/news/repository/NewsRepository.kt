package com.ericcbm.newsapp.components.news.repository

import com.ericcbm.newsapp.network.api.NewsApi
import com.ericcbm.newsapp.network.model.Headlines
import io.reactivex.Single

class NewsRepository(private val newsApi: NewsApi) {
    fun getHeadlines(): Single<Headlines> = newsApi.getHeadlines()
}