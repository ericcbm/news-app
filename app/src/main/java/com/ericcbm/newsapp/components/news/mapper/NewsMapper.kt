package com.ericcbm.newsapp.components.news.mapper

import com.ericcbm.newsapp.components.news.uimodel.NewsUiModel
import com.ericcbm.newsapp.network.model.Headlines
import com.ericcbm.newsapp.utils.DateUtils

class NewsMapper(private val dateUtils: DateUtils) {
    fun mapNewsList(headlines: Headlines): List<NewsUiModel> =
        headlines.articles.map { NewsUiModel(it.url, it.imageUrl, it.title, it.description, dateUtils.format(it.publishedAt), it.content) }
}
