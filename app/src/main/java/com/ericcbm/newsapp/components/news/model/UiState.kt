package com.ericcbm.newsapp.components.news.model

sealed class UiState {
    object ErrorUiState : UiState()
    object LoadingUiState : UiState()
}