package com.ericcbm.newsapp.components.news.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.ericcbm.newsapp.R
import com.ericcbm.newsapp.components.news.viewmodel.NewsListViewModel
import com.ericcbm.newsapp.components.news.adapter.NewsAdapter
import com.ericcbm.newsapp.components.news.model.UiState
import com.ericcbm.newsapp.components.news.uimodel.NewsUiModel
import com.ericcbm.newsapp.databinding.NewsListFragmentBinding
import org.koin.androidx.viewmodel.ext.android.viewModel

class NewsListFragment : Fragment(R.layout.news_list_fragment) {

    private var _binding: NewsListFragmentBinding? = null
    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    private val viewModel: NewsListViewModel by viewModel()
    private val newsAdapter by lazy { NewsAdapter { view, news -> onNewsClick(view, news) } }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = NewsListFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    private fun onNewsClick(view: View, news: NewsUiModel) {
        view.findNavController().navigate(NewsListFragmentDirections.actionNewsListFragmentToNewsDetailFragment(news))
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecycleView()
        initRetryButton()
        observeLiveDatas()
    }

    private fun initRetryButton() {
        binding.newsListTryAgainButton.setOnClickListener {
            viewModel.loadNewsList()
        }
    }

    private fun observeLiveDatas() {
        val progressBar = binding.newsListProgressBar
        val errorView = binding.newsListErrorView

        viewModel.run {
            newsList.observe(viewLifecycleOwner) {
                newsAdapter.submitList(it)
                progressBar.hide()
                errorView.visibility = GONE
            }
            uiState.observe(viewLifecycleOwner) {
                when (it) {
                    UiState.LoadingUiState -> {
                        errorView.visibility = GONE
                        progressBar.show()
                    }
                    UiState.ErrorUiState -> {
                        progressBar.hide()
                        errorView.visibility = VISIBLE
                    }
                    else -> { /* no-op */ }
                }
            }
        }
    }

    private fun initRecycleView() {
        binding.newsListRecyclerView.apply {
            adapter = newsAdapter
            layoutManager = LinearLayoutManager(context)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}