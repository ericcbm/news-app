package com.ericcbm.newsapp.components.news.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.ericcbm.newsapp.components.news.uimodel.NewsUiModel
import com.ericcbm.newsapp.databinding.NewsHeadlineBinding
import com.ericcbm.newsapp.databinding.NewsItemBinding

class NewsAdapter(private val onClick: (View, NewsUiModel) -> Unit): ListAdapter<NewsUiModel, NewsViewHolder>(NewsDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsViewHolder {
        val binding = NewsItemBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        val mergeBinding = NewsHeadlineBinding.bind(binding.root)
        return NewsViewHolder(binding, mergeBinding, onClick)
    }

    override fun onBindViewHolder(holder: NewsViewHolder, position: Int) {
        val news = getItem(position)
        holder.bind(news)
    }

    class NewsDiffCallback: DiffUtil.ItemCallback<NewsUiModel>() {
        override fun areItemsTheSame(oldItem: NewsUiModel, newItem: NewsUiModel): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(oldItem: NewsUiModel, newItem: NewsUiModel): Boolean {
            return oldItem.url == newItem.url
        }
    }
}