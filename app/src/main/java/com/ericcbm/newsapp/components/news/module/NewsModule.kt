package com.ericcbm.newsapp.components.news.module

import com.ericcbm.newsapp.components.news.interactor.NewsInteractor
import com.ericcbm.newsapp.components.news.mapper.NewsMapper
import com.ericcbm.newsapp.components.news.repository.NewsRepository
import com.ericcbm.newsapp.components.news.viewmodel.NewsDetailViewModel
import com.ericcbm.newsapp.components.news.viewmodel.NewsListViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val newsModule = module {
    single { NewsRepository(get()) }
    single { NewsMapper(get()) }
    single { NewsInteractor(get(), get()) }

    viewModel { NewsListViewModel(get()) }
    viewModel { NewsDetailViewModel(get()) }
}